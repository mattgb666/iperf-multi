# README #

This bash script can be run on a Linux machine to iteratively call iPerf and step through all combinations of useful iPerf parameters including multiple remote iPerf servers, running each test multiple times and then capturing the average result in a CSV file. 

### What is this repository for? ###

* To maintain an iperf-multi test script

### How do I get set up? ###

Copy this bash shell script onto a linux machine with iperf installed, that will act as the source of your iperf test. Run "iperf -s" on the remote servers you'll be testing to. Create a directory for log output and specify that directory in the $Log_Dir variable.

The script includes two variable sets, each of which can be called from the command line. Modify the variable set to suit your testing needs.

* Configuration
Modify your local copy of this script to set the following iperf parameters in either var_set1 or var_set2:

Test_Type="VXLAN_Edge"     # Name of test that will appear in column 1 of CSV file
Server_List="ubuntu-02 ubuntu-03"  # Space separated list of remote iperf servers running iperf -s
Flow_Time=10             # Number of seconds to run the iperf flow 
Parallel_Flows="1 10"    # Space separated list of number of parallel flows to run during each test
TCP_Windows="16KB 32KB 512KB"  # Space separated list of different TCP Window sizes you'd like to test with. 
MTUs="1500 8900"          # Space separated list of different MTU sizes you'd like to test with
Runs=3                 # Number of times a single iPerf test should be run before the average result is taken.


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines
